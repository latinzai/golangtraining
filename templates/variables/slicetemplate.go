package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("slicetpl.gohtml"))
}

func main() {
	err := tpl.Execute(os.Stdout, []string{"Sonic", "Mario", "Zelda", "Conquer"})
	if err != nil {
		log.Fatalln(err)
	}
}
