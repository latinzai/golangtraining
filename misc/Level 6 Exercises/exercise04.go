package main

type person struct {
	first string
	last  string
	age   int
}

func main() {
	p1 := person{
		first: "Jorge",
		last:  "Diaz",
		age:   25,
	}
	p1.speak()
}

func (p person) speak() {
	println("Hello! My name is", p.first, p.last, "and I'm", p.age, "years old")
}
