package main

import (
	"fmt"
	"strings"
)

var a int

type dog int

var myDog dog

func printAll(values ...string) {
	join := strings.Join(values, " - ")
	fmt.Println(join)
}
func main() {

	printAll("Perro", "Ballena", "Mosca", "Orangután", "Codorníz")

	a = 42
	//var_dump(a)
	myDog = 64
	//convertion
	a = int(myDog)

	//var_dump(string(myDog))
	//var_dump(a)

	// floatVar := 2.33
	// intVar := 5
	// stringVar := "Hello, World!"

	// var_dump(floatVar)
	// var_dump(intVar)
	// var_dump(stringVar)

	//array

	var myArr [5]int
	myArr[3] = 42
	//var_dump(myArr)

	//slices
	//mySlice := []type{values} (COMPOSITE LITERALS)
	mySlice := []int{1, 2, 3, 4}
	slicedSlice := mySlice[1:3]
	slicedSlice = append(slicedSlice, 4, 5)
	//var_dump(slicedSlice)

	businessDays := []string{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"}
	weekendDays := []string{"Saturday", "Sunday"}

	var days = append(businessDays, weekendDays...)

	for _, v := range days {
		println(v)
	}

	// for _, value := range mySlice {
	// 	var_dump(value)
	// }

	myMap := map[string]int{
		"Jorge": 25,
		"Clara": 27,
	}

	myMap["Martha"] = 23
	myMap["Manuel"] = 12

	if v, ok := myMap["Manuel"]; ok {
		println(v)
	}

	delete(myMap, "Manuel")

	for key, value := range myMap {
		println(key, " => ", value)
	}

	type animal struct {
		species string
		name    string
	}
	type person struct {
		firstname string
		lastname  string
		pet       animal
	}

	type secretAgent struct {
		person
		licenseToKill bool
	}

	person1 := person{
		firstname: "Jorge",
		lastname:  "Diaz",
		pet: animal{
			species: "Felines",
			name:    "Mishu",
		},
	}

	//anonimous struct

	dog := struct {
		name   string
		weight float32
	}{
		name:   "Fiddo",
		weight: 40.5,
	}

	fmt.Println(dog)

	fmt.Println(person1.firstname, person1.lastname, "owns a", person1.pet.species, "named", person1.pet.name)

	agent := secretAgent{
		person: person{
			firstname: "Laura",
			lastname:  "Alvarado",
		},
		licenseToKill: true,
	}

	fmt.Println("Hi. I'm", agent.lastname + ",", agent.firstname, agent.lastname)

}

func assertType(x interface{}) {
	switch x.(type) {
	case int:
		fmt.Println("I'm an integer with value", x.(int))
	case string:
		fmt.Println("I'm a string with value", x.(string))

	}
}
