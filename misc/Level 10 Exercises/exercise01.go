package main

import (
	"fmt"
)

func main() {
	c := make(chan int, 2)

	go func(channel chan int) {
		channel <- 42
		channel <- 777
	}(c)

	fmt.Println(<-c)
	fmt.Println(<-c)
}
