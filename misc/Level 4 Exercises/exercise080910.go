package main

import (
	"fmt"
	"strings"
)

func main() {

	//create map
	myMap := map[string][]string{
		"diaz_jorge":     {"Apex Legends", "Assassin's Creed"},
		"abreu_gael":     {"Dolcerie", "Chinita (dique)", "Last of Us 2", "Flutter"},
		"cedano_cesar":   {"Diving", "GoLang", "Vue.js", "node.js"},
		"leon_alexander": {"Pao", "La Fuerza"},
	}

	//delete
	delete(myMap, "diaz_jorge")

	//add
	myMap["casado_frankell"] = []string{"Just gaming"}

	for i, v := range myMap {
		firstname := strings.Split(i, "_")[1]
		lastname := strings.Split(i, "_")[0]
		fullname := fmt.Sprint(strings.Title(firstname), " ", strings.Title(lastname))
		fmt.Println(fullname, "likes the following things:")
		for k, thing := range v {
			fmt.Printf("\t %v. %v \n", k, thing)
		}
	}
}
