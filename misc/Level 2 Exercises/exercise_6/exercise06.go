package main

func main() {

	const (
		currentYear = 2019
		a           = currentYear + iota
		b           = currentYear + iota
		c           = currentYear + iota
		d           = currentYear + iota
	)

	println(currentYear)
	println(a)
	println(b)
	println(c)
	println(d)
}
