package example

import (
	"fmt"
)

func ExampleHello() {
	Hello()
	// Output: zxczcx
}

func Hello() {
	fmt.Println("hello")
}
