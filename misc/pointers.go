package main

func main() {
	a := 42
	b := &a
	*b = 22
	println(a)
}
