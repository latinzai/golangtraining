package main

import "fmt"

type person struct {
	name string
	age  int
}

type human interface {
	speak()
}

func (p *person) speak() {
	fmt.Println("My name is", p.name, "and I'm", p.age, "years old")
}

func saySomething(h human) {
	h.speak()
}
func main() {
	p := person{
		name: "Jorge Diaz",
		age:  25,
	}

	//you CAN pass a value of type *person into saySomething
	saySomething(&p)

	//you CANNOT pass a value of type person into saySomething
	//saySomething(p)
}
