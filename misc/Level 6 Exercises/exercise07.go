package main

func main() {

	getHelloMessage := func() string {
		return "Hello there!"
	}

	func() {
		println(getHelloMessage())
	}()
}
