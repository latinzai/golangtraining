package main

import "fmt"

func main() {
	c := make(chan int, 2)

	c <- 2
	c <- 3
	c <- 4

	fmt.Println(<-c)
	fmt.Println(<-c)
	fmt.Println(<-c)

	// for v := range c {
	// 	fmt.Println(v)
	// }
}
