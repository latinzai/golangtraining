package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	printSpecs()

	var wg sync.WaitGroup

	wg.Add(2)

	go func() {
		fmt.Println("I'm from routine 1")
		wg.Done()
	}()

	go func() {
		fmt.Println("I'm from routine 2")
		wg.Done()
	}()

	printSpecs()

	wg.Wait()

	printSpecs()
}

func printSpecs() {
	fmt.Println("--------------------------------------")
	fmt.Println("mid cpu", runtime.NumCPU())
	fmt.Println("mid gs", runtime.NumGoroutine())
	fmt.Println("--------------------------------------")
}
