package mystr

import "strings"

func Cat(xs []string) string {
	s := xs[0]
	for _, v := range xs[1:] {
		s += " " + v
	}
	return s
}

func Join(xs []string) string {
	return strings.Join(xs, " ")
}
