package main

import (
	"fmt"
	"sync"
)

func main() {

	var wg sync.WaitGroup
	var mutex sync.Mutex
	const routineSize int = 100
	var counter int = 0

	wg.Add(routineSize)

	for i := 0; i < routineSize; i++ {
		go func() {
			mutex.Lock()
			v := counter
			v++
			counter = v
			fmt.Println("counter:", counter)
			mutex.Unlock()
			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Println("counter:", counter)
}
