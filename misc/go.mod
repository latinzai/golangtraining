module mymain

go 1.13

require (
	github.com/GoesToEleven/go-programming v0.0.0-20181228215051-5519dbb87bc5
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550 // indirect
	rsc.io/quote/v3 v3.1.0
)
