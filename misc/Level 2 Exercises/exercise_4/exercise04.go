package main

import "fmt"

func main() {
	x := 5
	fmt.Printf("Binary: %b\n", x)
	fmt.Printf("Hexadecimal: %#X\n", x)
	fmt.Printf("Decimal: %d\n", x)

	y := x << 1

	fmt.Printf("Binary: %b\n", y)
	fmt.Printf("Hexadecimal: %#X\n", y)
	fmt.Printf("Decimal: %d\n", y)
}
