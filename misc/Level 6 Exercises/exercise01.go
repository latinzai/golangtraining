package main

func main() {
	foo := foo()
	intBar, strBar := bar()
	println(foo, " ", intBar, " ", strBar)
}

func foo() int {
	return 42
}

func bar() (int, string) {
	return 777, "Hello, World"
}
