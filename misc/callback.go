package main

import "fmt"

func main() {

	numbers := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	total := filterAndSum(discriminateEvenNumbers, numbers...)
	fmt.Println("The total is", total)
	total = filterAndSum(discriminateOddNumbers, numbers...)
	fmt.Println("The total is", total)
}

func filterAndSum(filterFunction func(numbersToFilter ...int) []int, numbers ...int) int {

	total := 0
	for _, v := range filterFunction(numbers...) {
		total += v
	}
	return total
}

func discriminateOddNumbers(numbers ...int) []int {
	var numbersFiltered []int

	for _, v := range numbers {
		if v%2 == 0 {
			numbersFiltered = append(numbersFiltered, v)
		}
	}

	return numbersFiltered
}

func discriminateEvenNumbers(numbers ...int) []int {
	var numbersFiltered []int

	for _, v := range numbers {
		if v%2 != 0 {
			numbersFiltered = append(numbersFiltered, v)
		}
	}

	return numbersFiltered
}
