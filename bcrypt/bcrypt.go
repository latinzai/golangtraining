package main

import (
	"fmt"

	"golang.org/x/crypto/bcrypt"
)

func main() {
	pwd := "password123"
	bs, err := bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.MinCost)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Your encrypted password is", bs)

	err = bcrypt.CompareHashAndPassword(bs, []byte(pwd))
}
