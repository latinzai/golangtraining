package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

func main() {
	wg.Add(1)
	foo()
	go bar()
	wg.Wait()
	// go foo()
	// go bar()
}
func foo() {
	for i := 0; i < 20; i++ {
		fmt.Println("foo:", i)
	}
}

func bar() {
	for i := 0; i < 20; i++ {
		fmt.Println("bar:", i)
	}

	wg.Done()
}
