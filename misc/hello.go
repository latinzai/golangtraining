package main

import "rsc.io/quote/v3"

//Hello ... Says hello to the world!.
func Hello() string {
	return quote.HelloV3()
}

//Proverb ... Says a pithy message.
func Proverb() string {
	return quote.Concurrency()
}
