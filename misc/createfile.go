package main

import (
	"io"
	"os"
	"strings"
)

func main() {
	f, err := os.Create("names.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	r := strings.NewReader("Jorgito Diaz")
	io.Copy(f, r)
}
