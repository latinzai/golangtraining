package main

import (
	"fmt"
	"sort"
	"time"
)

/*
Given Counter slices that are sorted by Time, merge the slices into one slice.
Make sure that counters with the same Time are merged and the Count is increased.
E.g:

A = [{Time: 0, Count: 5}, {Time:1, Count: 3}, {Time: 4, Count 7}]
B = [{Time: 1, Count: 9}, {Time:2, Count: 1}, {Time: 3, Count 3}]

merge(A, B) ==>

[{Time: 0, Count: 5}, {Time: 1: Count: 12}, {Time: 2, Count 1}, {Time: 3, Count: 3}, {Time: 4: Count: 7}]

Explain the efficiency of your merging.
*/

type Counter struct {
	Time  time.Time
	Count uint64
}

var (
	A = []Counter{
		{Time: time.Unix(0, 0), Count: 5},
		{Time: time.Unix(0, 1), Count: 3},
		{Time: time.Unix(0, 4), Count: 7},
	}

	B = []Counter{
		{Time: time.Unix(0, 0), Count: 5},
		{Time: time.Unix(0, 1), Count: 12},
		{Time: time.Unix(0, 2), Count: 1},
		{Time: time.Unix(0, 3), Count: 3},
		{Time: time.Unix(0, 4), Count: 7},
	}
)

func (r Counter) String() string {
	return fmt.Sprintf("{Time: %d, Count: %d}", r.Time.Nanosecond(), r.Count)
}

// Merge ... Merges.
func Merge(a, b []Counter) []Counter {

	// Concatenate both slices into one.
	xAB := append(a, b...)

	// Sort the resulting slice.
	sort.Slice(xAB, func(i, j int) bool {
		return xAB[i].Time.Nanosecond() < xAB[j].Time.Nanosecond()
	})
	result := func(counters []Counter) []Counter {
		var output []Counter
		for i := range counters {
			if output == nil || len(output) == 0 {
				output = append(output, counters[i])
			} else {
				isDuplicate := false
				for j := range output {
					if output[j].Time == counters[i].Time {
						//fmt.Println("a:", output[j].Count, "b:", counters[i].Count)
						output[j].Count += counters[i].Count
						isDuplicate = true

					}
				}
				if !isDuplicate {
					output = append(output, counters[i])
				}
			}
		}
		return output
	}(xAB)

	return result
}

func main() {
	AB := Merge(A, B)
	for _, c := range AB {
		fmt.Println(c)
	}
}
