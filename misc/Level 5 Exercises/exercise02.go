package main

import (
	"fmt"
	"strings"
)

type person struct {
	firstname              string
	lastname               string
	favoriteIceCreamFlavor []string
}

func (p person) FullName() string {
	return fmt.Sprint(p.firstname, " ", p.lastname)
}

func (p person) getFavoriteIceCream() string {
	return strings.Join(p.favoriteIceCreamFlavor, ", ")
}

func (p person) getMapKey() string {
	return strings.ToLower(fmt.Sprintf("%v_%v", p.lastname, p.firstname))
}

func (p person) toString() string {
	return p.FullName() + " likes eating ice cream with the following flavors: " + p.getFavoriteIceCream()
}

func main() {

	p1 := person{
		firstname:              "Jorge",
		lastname:               "Diaz",
		favoriteIceCreamFlavor: []string{"Chocolate Chips", "Dulce de Leche", "Cake"},
	}

	p2 := person{
		firstname:              "Clara",
		lastname:               "Sisa",
		favoriteIceCreamFlavor: []string{"Winx", "Picapollo", "Sakura", "Engombe"},
	}

	people := map[string]person{
		p1.getMapKey(): p1,
		p2.getMapKey(): p2,
	}

	for k, v := range people {
		fmt.Printf("%v => %v\n", k, v.toString())
	}

	println(p1.toString())
	println(p2.toString())
}
