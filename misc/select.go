package main

import "fmt"

func main() {
	even := make(chan int)
	odd := make(chan int)
	quit := make(chan bool)
	go send(even, odd, quit)
	receive(even, odd, quit)
	fmt.Println("About to exit...")
}

func receive(even, odd <-chan int, quit <-chan bool) {
	for {
		select {
		case v := <-even:
			fmt.Println("From the even channel:", v)
		case v := <-odd:
			fmt.Println("From the odd channel:", v)
		case <-quit:
			return
		}
	}
}

func send(even, odd chan<- int, quit chan<- bool) {

	for index := 0; index < 100; index++ {
		if index%2 == 0 {
			even <- index
		} else {
			odd <- index
		}

	}
	close(quit)
}
