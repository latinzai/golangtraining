package main

import (
	"html/template"
	"log"
	"net/http"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("sayhi.gohtml"))
}

func main() {
	http.HandleFunc("/", foo)
	http.HandleFunc("/dog/", bar)
	http.HandleFunc("/me/", myName)
	http.ListenAndServe(":8080", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {
	data := struct {
		Name    string
		Species string
	}{
		"Index",
		"Page",
	}

	err := tpl.Execute(w, data)
	HandleError(w, err)
}

func bar(w http.ResponseWriter, req *http.Request) {
	data := struct {
		Name    string
		Species string
	}{
		"Chiquito",
		"dog",
	}

	err := tpl.Execute(w, data)
	HandleError(w, err)
}

func myName(w http.ResponseWriter, req *http.Request) {
	data := struct {
		Name    string
		Species string
	}{
		"Jorge",
		"human",
	}

	err := tpl.Execute(w, data)
	HandleError(w, err)
}

func HandleError(w http.ResponseWriter, err error) {
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Fatalln(err)
	}
}
