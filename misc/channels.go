package main

import (
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup

func main() {

	wg.Add(1)
	//Directional channel
	channel := make(chan int)
	// channelReceive := make(<-chan int) //receive
	// channelSend := make(chan<- int)    //send

	go sendSomething(channel)

	receiveSomething(channel)
	wg.Wait()
	fmt.Println("About to exit...")

	go func() {
		for i := 0; i < 5; i++ {
			channel <- i
		}
		close(channel)
	}()

	for v := range channel {
		fmt.Println(v)
	}
	// fmt.Println("--------------")
	// fmt.Printf("c\t%T\n", channel)
	// fmt.Printf("cr\t%T\n", channelReceive)
	// fmt.Printf("cs\t%T\n", channelSend)
}

func sendSomething(c chan<- int) {
	time.Sleep(5 * time.Second)
	c <- 42
	time.Sleep(5 * time.Second)
	fmt.Println("sendSomething: I'm done")
	wg.Done()
}

func receiveSomething(c <-chan int) {
	fmt.Println(<-c)
}
