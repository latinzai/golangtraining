package main

import (
	"fmt"
)

func main() {
	c := make(chan int)

	go func(chn chan<- int) {
		chn <- 1
	}(c)

	v, ok := <-c

	fmt.Println(v, ok)

	close(c)

	v, ok = <-c
	fmt.Println(v, ok)
}
