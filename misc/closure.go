package main

type animal struct {
	name    string
	getName func()
}

func incrementor() func() int {
	var x int
	return func() int {
		x++
		return x
	}
}
