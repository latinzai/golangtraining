package main

import "fmt"

func main() {

	anonymous := struct {
		name string
		age  int
	}{
		name: "Jorge",
		age:  2,
	}

	fmt.Print(anonymous)
}
