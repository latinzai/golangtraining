package main

import "math"

type shape interface {
	calculateArea() float64
}

type circle struct {
	radius float64
}

type rectangle struct {
	length float64
	width  float64
}

func (c circle) calculateArea() float64 {
	return math.Pow(c.radius, 2) * math.Pi
}

func (c rectangle) calculateArea() float64 {
	return c.length * c.width
}

func displayArea(s shape) {
	println("The area of this shape is", s.calculateArea())
}

func main() {

	circle := circle{
		radius: 14.53434,
	}

	square := rectangle{
		width:  5.43,
		length: 5.43,
	}

	displayArea(circle)
	displayArea(square)
}
