package main

import (
	"fmt"
	"sort"
	"time"
)

/*
Given Counter slices that are sorted by Time, merge the slices into one slice.
Make sure that counters with the same Time are merged and the Count is increased.
E.g:

A = [{Time: 0, Count: 5}, {Time:1, Count: 3}, {Time: 4, Count 7}]
B = [{Time: 1, Count: 9}, {Time:2, Count: 1}, {Time: 3, Count 3}]

merge(A, B) ==>

[{Time: 0, Count: 5}, {Time: 1: Count: 12}, {Time: 2, Count 1}, {Time: 3, Count: 3}, {Time: 4: Count: 7}]

Explain the efficiency of your merging.
*/

type Counter struct {
	Time  time.Time
	Count uint64
}

var (
	A = []Counter{
		{Time: time.Unix(0, 0), Count: 5},
		{Time: time.Unix(0, 1), Count: 3},
		{Time: time.Unix(0, 4), Count: 7},
	}

	B = []Counter{
		{Time: time.Unix(0, 0), Count: 5},
		{Time: time.Unix(0, 1), Count: 12},
		{Time: time.Unix(0, 2), Count: 1},
		{Time: time.Unix(0, 3), Count: 3},
		{Time: time.Unix(0, 4), Count: 7},
	}
)

func Merge(a, b []Counter) []Counter {
	AB := append(a, b...)
	acc := reduce([]Counter{}, AB)
	return acc
}

func reduce(accumulator, currentArray []Counter) []Counter {
	if len(currentArray) < 1 {
		return accumulator
	}
	currentElement := currentArray[0]
	var currentElementExists bool
	for i, e := range accumulator {
		if e.Time == currentElement.Time {
			accumulator[i].Count += currentElement.Count
			currentElementExists = true
			break
		}
	}
	if !currentElementExists {
		accumulator = append(accumulator, currentElement)
	}
	if len(currentArray) > 0 {
		return reduce(accumulator, currentArray[1:])
	}
	return accumulator
}

func main() {
	AB := Merge(A, B)

	sort.SliceStable(AB, func(i, j int) bool {
		return AB[i].Time.Before(AB[j].Time)
	})

	for _, c := range AB {
		fmt.Println(c)
	}

}
