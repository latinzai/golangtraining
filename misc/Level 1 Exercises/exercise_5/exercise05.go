package main

import "fmt"

type dog string

var y string

var x dog

func main() {
	fmt.Println(x)
	fmt.Printf("%T\n", x)
	x = "I'm 64 years old."
	fmt.Println(x)
	y = string(x)
	fmt.Println(y)
	fmt.Printf("%T\n", y)
}
