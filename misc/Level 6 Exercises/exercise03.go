package main

func main() {
	defer foo()
	println("Hello")
}

func foo() {
	defer func() {
		println("foo DEFER ran")
	}()
	println("foo ran")
}
