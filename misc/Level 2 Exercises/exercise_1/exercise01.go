package main

import "fmt"

func main() {
	x := 5
	fmt.Printf("Binary: %b\n", x)
	fmt.Printf("Hexadecimal: %#X\n", x)
	fmt.Printf("Decimal: %d\n", x)
}
