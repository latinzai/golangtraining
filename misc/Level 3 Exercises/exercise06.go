package main

import (
	"fmt"
	"os"
)

func main() {

	var name string = os.Args[1]

	if x := name; x == "Jorge Diaz" {
		printName(x)
	} else if name == "Clara Elena" {
		printName(x)
	} else {
		fmt.Println("I don't know you,", name)
	}
}

func printName(name string) {
	fmt.Println("Your name is", name)
}
