package main

import (
	"fmt"
)

func main() {
	var answer1, answer2, answer3 string

	answer1 = scanText("Name")
	answer2 = scanText("Favorite Food")
	answer3 = scanText("Favorite Sport")

	fmt.Println(answer1, answer2, answer3)
}

func scanText(fieldName string) string {
	var scannedText string
	fmt.Printf("%s: ", fieldName)
	_, err := fmt.Scan(&scannedText)
	if err != nil {
		panic(err)
	}
	return scannedText
}
