package main

import (
	"fmt"

	"bitbucket.org/latinzai/dog"
)

func main() {
	dogYears := dog.Years(4)
	fmt.Println(dogYears)
}
