package dog

import (
	"testing"

	"github.com/GoesToEleven/go-programming/code_samples/010-ninja-level-thirteen/01/finished/dog"
)

func TestYearsTwo(t *testing.T) {
	inputYears := 3
	expected := 21

	if got := dog.YearsTwo(3); expected != got {
		t.Error(
			"For input", inputYears,
			"Expected", expected,
			"got", got,
		)
	}
}
