package main

import "fmt"

var startNo int = 0
var endNo int = 100
var xi []int

func main() {

	for i := startNo; i <= endNo; i++ {
		if isPrime(i) {
			xi = append(xi, i)
		}
	}

	for _, v := range xi {
		fmt.Println(v)
	}

}

func isPrime(number int) bool {

	if (number & 1) == 0 {
		return number == 2
	}

	for i := 3; (i * i) <= number; i += 2 {
		return !((number % i) == 0)
	}

	return number != 1
}
