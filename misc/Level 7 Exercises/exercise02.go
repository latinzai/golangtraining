package main

import "fmt"

type person struct {
	name string
	age  int
}

func main() {
	jorge := person{
		name: "Jorge",
		age:  25,
	}
	fmt.Println(jorge)
	p1 := &jorge
	changeMe(p1)
	fmt.Println(jorge)
}

func changeMe(p *person) {
	p.name = "Marcos"
	p.age = 36
}
