package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"text/template"
	"time"
)

type radioButton struct {
	Name       string
	Value      string
	IsDisabled bool
	IsChecked  bool
	Text       string
}
type pageVariables struct {
	Date         string
	Time         string
	PageTitle    string
	RadioButtons []radioButton
	Answer       string
}

func userSelected(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	yourAnimal := r.Form.Get("animalselect")

	pageVars := pageVariables{
		PageTitle: "Your preferred animal",
		Answer:    yourAnimal,
	}

	view(w, "select", pageVars)
}
func displayRadioButtons(w http.ResponseWriter, r *http.Request) {

	radioButtons := []radioButton{
		{"animalselect", "cats", false, false, "Cats"},
		{"animalselect", "dogs", false, false, "Dogs"},
	}

	pageVars := pageVariables{
		PageTitle:    "Which do you prefer?",
		RadioButtons: radioButtons,
	}

	view(w, "select", pageVars)

}

func today(w http.ResponseWriter, r *http.Request) {
	now := time.Now()
	pageVars := pageVariables{
		Date: now.Format("02-01-2006"),
		Time: now.Format("15:04:05"),
	}

	view(w, "today", pageVars)
}

func view(w http.ResponseWriter, templateName string, data pageVariables) {

	templateFile := fmt.Sprintf("./templates/%s.gohtml", templateName)
	t, err := template.ParseFiles(templateFile)
	tryHandleError(err, "Template parsing error")
	err = t.Execute(w, data)
	tryHandleError(err, "Template executing error")
}

func tryHandleError(err error, errorMessages ...string) {

	if err != nil {
		errorMessage := strings.Join(errorMessages, " ") + err.Error()
		log.Print(errorMessage)
	}
}

func helloWorld(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World")
}

func main() {
	http.HandleFunc("/", helloWorld)
	http.HandleFunc("/today", today)
	http.HandleFunc("/selectanimal", displayRadioButtons)
	http.HandleFunc("/displayresult", userSelected)
	http.ListenAndServe(":8080", nil)
}
