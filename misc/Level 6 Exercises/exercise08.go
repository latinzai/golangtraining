package main

func main() {

	getHelloMessage := getHelloFunc()

	println(getHelloMessage())
}

func getHelloFunc() func() string {
	return func() string {
		return "Hello there!"
	}
}
