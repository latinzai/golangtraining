package main

/*
We have servers that keep count of pings. For some reason, just one of them is
getting pings.
TODO: fix the problem.
Extra credits: explain what properties of this code change with your solution.
*/

import (
	"fmt"
	"sync"
	"time"

	term "github.com/nsf/termbox-go"
)

type location int

var quit (chan bool)

const (
	sfo location = iota
	ams
	lhr
)

var servers = map[location](chan int){
	sfo: make(chan int),
	ams: make(chan int),
	lhr: make(chan int),
}
var wg sync.WaitGroup
var mutex sync.Mutex

func main() {
	quit = make(chan bool)
	err := term.Init()
	if err != nil {
		panic(err)
	}

	defer term.Close()
	go readExitKey()
	go pingDispatcher()
	report()
}

func pingDispatcher() {
	for server := range servers {
		go ping(server)
	}
}

func report() {
	for range time.Tick(100 * time.Millisecond) {
		fmt.Printf("\rPings: [SFO: %-6d | AMS: %-6d | LHR: %-6d ]", <-servers[sfo], <-servers[ams], <-servers[lhr])
		if <-quit {
			fmt.Println("\nEXIT")
			return
		}
	}
}

func ping(c location) {
	i := 1
	for {
		time.Sleep(2 * time.Millisecond)
		servers[c] <- i
		i++
	}
}

func readExitKey() {
	for {
		switch ev := term.PollEvent(); ev.Type {
		case term.EventKey:
			switch ev.Key {
			case term.KeyEsc:
				quit <- true
				return
			}
		}
		quit <- false
	}
}
