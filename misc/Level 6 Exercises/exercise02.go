package main

func main() {

	xi := []int{5, 5, 6, 4, 8}
	println(foo(xi...))
	println(bar(xi))
}

func foo(xi ...int) int {
	return bar(xi)
}

func bar(xi []int) int {
	var result int

	for _, v := range xi {
		result += v
	}

	return result
}
