package main

import "fmt"

func main() {
	i := get3()
	fmt.Println(i)
}

func get3() int {
	i := 2
	defer func() {
		i++
	}()
	return i
}
