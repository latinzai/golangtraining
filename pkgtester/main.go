package main

import (
	"fmt"
	"strings"

	"bitbucket.org/latinzai/mystr"
)

func main() {
	s := "Cuando estás inspirado por algún gran propósito, por algún extraordinario proyecto, los pensamientos rompen las barreras; la mente trasciende sus limitaciones, la conciencia se expande en todas direcciones y te encuentras en un nuevo mundo maravilloso. Las fuerzas, las facultades y los talentos dormidos cobran vida. En ese momento te das cuenta de que eres mucho más grande de lo que jamás hubieras soñado. -Patañjali"
	xs := strings.Split(s, " ")

	for _, v := range xs {
		fmt.Println(v)
	}

	result := mystr.Cat(xs)
	fmt.Println(result)
}
