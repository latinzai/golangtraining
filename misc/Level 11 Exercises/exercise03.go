package main

import "fmt"

type customErr struct {
	info string
}

func (err *customErr) Error() string {
	return fmt.Sprintf("Could not perform the operation: %s", err.info)
}

func foo(err error) {
	fmt.Println(err.Error())
}

func main() {

	if true {
		err := &customErr{"This will always evaluate to true!"}
		foo(err)
	}
}
