package main

import (
	"errors"
	"fmt"
)

func main() {

	e := errors.New("InvalidOperationException")
	n, err := fmt.Println(e.Error())
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(n)
}
