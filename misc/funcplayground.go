package main

type animal struct {
	name    string
	getName func()
}

func main() {

	a := animal{
		name: "LOL",
		getName: func() {
			println("LOL")
		},
	}

	a.getName()
}
