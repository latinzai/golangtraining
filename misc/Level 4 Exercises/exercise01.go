package main

import "fmt"

func main() {
	myArray := [5]int{1, 2, 3, 4, 5}

	for i, v := range myArray {
		println(i, " => ", v)
	}

	fmt.Printf("%T\n", myArray)
}
