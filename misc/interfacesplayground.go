package main

import "fmt"

type human interface {
	eat()
}

type animal interface {
	eat()
}

type livingThing struct {
	name string
}

func (l livingThing) eat() {
	fmt.Println("I'm eating...")
	fmt.Printf("%T\n", l)
}

func forAnimals(a animal) {
	a.eat()
}

func forHumans(h human) {
	h.eat()
}
func main() {
	thing := livingThing{
		name: "Dog",
	}

	thing.eat()

	forAnimals(thing)
	forHumans(thing)

}
