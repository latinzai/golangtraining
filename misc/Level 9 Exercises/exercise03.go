package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	var wg sync.WaitGroup
	const routineSize int = 100
	var counter int = 0

	wg.Add(routineSize)

	for i := 0; i < routineSize; i++ {
		go func() {
			v := counter
			runtime.Gosched()
			v++
			counter = v
			fmt.Println("counter:", counter)
			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Println("counter:", counter)
}
