package dumper

import "fmt"

//VarDump ...This function will print out the type and value of a variable.
func VarDump(variable interface{}) {
	println("\n")
	fmt.Printf("Value: %v\n", variable)
	fmt.Printf("Type: %T\n", variable)
	fmt.Printf("Binary: %b\n", variable)
	fmt.Printf("Hex: %x\n", variable)
	fmt.Printf("Hex-2: %#x\n", variable)
	println("\n")
}

func stringToBin(s string) (binString string) {
	for _, c := range s {
		binString = fmt.Sprintf("%s%b", binString, c)
	}
	return binString
}
