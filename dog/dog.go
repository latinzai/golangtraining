// Package dog contains a set of tools used to know details about dogs.
package dog

// Years function takes human years as arguments and turn them into dog years.
func Years(humanYears int) int {
	return humanYears * 7
}
