package main

func main() {
	const pi = 3.1416
	const myName string = "Jorge Diaz"

	println(pi)
	println(myName)
}
