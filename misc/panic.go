package main

import (
	"errors"
	"fmt"
)

func main() {
	foo()
}

func foo() {
	defer fmt.Println("I deferred from foo()")
	bar()
	fmt.Println("I called bar()")
}

func bar() {
	err := errors.New("This is an awful error")
	defer fmt.Println("I deferred from bar()")
	panic(err)
	fmt.Println("I've panic'd from bar()")
}
