package main

import (
	"fmt"
	"runtime"
)

func main() {
	os := runtime.GOOS
	arch := runtime.GOARCH
	fmt.Println("OPERATING SYSTEM:", os)
	fmt.Println("PROCESSOR ARCHITECTURE:", arch)
}
