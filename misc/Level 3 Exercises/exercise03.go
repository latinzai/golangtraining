package main

import (
	"fmt"
	"time"
)

func main() {

	var currentYear = time.Now().Year()
	var yearOfBirth int = 1994

	for yearOfBirth <= currentYear {
		fmt.Println("I was still alive in", yearOfBirth)
		yearOfBirth++
	}
}
