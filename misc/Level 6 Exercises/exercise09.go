package main

import "strings"

func main() {

	countWordsFunc := func(str string) {
		println(len(str))
	}

	splitString := func(str string) {
		slice := strings.Split(str, " ")

		for _, v := range slice {
			println(v)
		}
	}

	manipulateString(splitString, "El Mayor Clásico tiene que ponerse las pilas.")
	manipulateString(countWordsFunc, "This message should be 38 spaces long.")
}

func manipulateString(manipulatingFunc func(str string), stringToManipulate string) {
	manipulatingFunc(stringToManipulate)
}
